import numpy as np


class Cosine:

    attribute = "Cosine"

    def __init__(self, a, b):
        # manually compute cosine similarity
        dot_product = np.dot(a, b)
        norm_a = np.linalg.norm(a)
        norm_b = np.linalg.norm(b)
        self.cos = dot_product / (norm_a * norm_b)

    def getResult(self):
        #Get cosine Smilarity
        return self.cos