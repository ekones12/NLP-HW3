import pandas as pd
import numpy as np
from tqdm import tqdm
from gensim.models import Doc2Vec
from sklearn.linear_model import LogisticRegression
from gensim.models.doc2vec import TaggedDocument
import nltk

class ReadFile(object):
    # Class Attribute
    species = "Read Fİle"

    # Initializer / Instance Attributes
    def __init__(self, path,taglist):
        file = open(path, "r+")
        self.sentences = file.read()
        file.close()
        self.taglist = taglist
        self.tagDict = dict()
        self.tagSplitter()

    def tagSplitter(self):
        last_tag = ''
        for line in self.sentences.split('\n'):
            words = line.split(" ")
            words = list(map(str.strip, words))
            if words[0] == ":" and words[1] in self.taglist:
                last_tag = words[1]
            elif last_tag in self.taglist:
                self.tagDict .get(last_tag, self.tagDict.setdefault(last_tag, [])).append(words)

    def getTagDict(self):
        return self.tagDict

class ReadCsv:

    #For read csv

    def __init__(self,FILEPATH):
        df = pd.read_csv(FILEPATH).drop(['Unnamed: 0',"movieId"], axis=1)
        df = df[['plot', "tag"]]
        df = df[pd.notnull(df['plot'])]
        df.index = range(df.shape[0])
        self.df = df

    def getDataFrame(self):
        return self.df


class Doc2VecModel:


    def __init__(self,dfObject):
        tqdm.pandas(desc="progress-bar")
        df = dfObject.getDataFrame()
        self.train = df[:2000]
        self.test = df[2000:]
    nltk.stem
    def DocumantTagger(self):

        self.train_tagged = self.train.apply(
            lambda r: TaggedDocument(words= nltk.word_tokenize(r['plot']), tags=[r.tag]), axis=1)
        self.test_tagged = self.test.apply(
            lambda r: TaggedDocument(words= nltk.word_tokenize(r['plot']), tags=[r.tag]), axis=1)

    def CreateModelVocab(self):
        self.model = Doc2Vec(dm=0, vector_size=50, negative=5, hs=0, min_count=2, sample=0)
        self.model.build_vocab([x for x in tqdm(self.train_tagged.values)])

    def TrainModel(self,epoch):
        for epoch in range(epoch):
            self.model.train([x for x in tqdm(self.train_tagged.values)], total_examples=len(self.train_tagged.values),
                             epochs=1)

    def getVectors(self,model, tagged_docs):
        sents = tagged_docs.values
        targets, regressors = zip(*[(doc.tags[0], model.infer_vector(list(doc.words), steps=1)) for doc in sents])
        return targets, regressors

    def Regression(self):
        y_train, X_train = self.getVectors(self.model, self.train_tagged)
        y_test, X_test = self.getVectors(self.model, self.test_tagged)
        logreg = LogisticRegression(n_jobs=1, C=1e5)
        logreg.fit(X_train, y_train)
        y_pred = logreg.predict(X_test)
        return y_pred , y_test


    def Accuracy(self,predicted,real):
        accuracy = (np.array(predicted) == np.array(real)).sum()
        print(accuracy * 100 / len(real))

