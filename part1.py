import gensim.models.keyedvectors as word2vec
from cosine import Cosine
from Models import ReadFile

taglist = ['capital-world', 'currency', 'city-in-state', 'family', 'gram1-adjective-to-adverb',
           'gram2-opposite', 'gram3-comparative', 'gram6-nationality-adjective']


def prediction(words, prediction, model):
    result = -2
    last_key = ""
    vocab = model.vocab.keys()
    for i in vocab:
        temp_result = Cosine(prediction, model[i]).getResult()
        if temp_result > result and i != words[0] and i != words[1] and i != words[2]:
            result = temp_result
            last_key = i
    return last_key, result


if __name__ == '__main__':
    model = word2vec.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, limit=5000)
    sentences = ReadFile("word-test.txt", taglist).getTagDict()
    for tag, wordlist in sentences.items():
        tag_accuracy = 0
        current_sentence_size = 0
        total_len = len(wordlist)
        for words in wordlist:
            try:
                vectoral_sum = model[words[1]] - model[words[0]] + model[words[2]]
                predicted_tag, smilarity = prediction(words, vectoral_sum, model)
                if words[3] == predicted_tag:
                    tag_accuracy += 1
                current_sentence_size += 1
                #print("Actual : [" + words[3] + '] Predicted: [' + predicted_tag + '] Prediction Cosine Smilarity: ',
                     # smilarity, "     -->Currently Calculated Accuracy:", (tag_accuracy * 100) / current_sentence_size)
            except:
                total_len -= 1
        print("Accuracy for [" + tag + '] =', (tag_accuracy * 100) / total_len)
