
from Models import ReadCsv,Doc2VecModel

trainObject = ReadCsv('tagged_plots_movielens.csv')

doc2vec = Doc2VecModel(trainObject)

doc2vec.DocumantTagger()

doc2vec.CreateModelVocab()

doc2vec.TrainModel(30) # with 30 epoch

predictedList, realList = doc2vec.Regression()

doc2vec.Accuracy(predictedList, realList)

